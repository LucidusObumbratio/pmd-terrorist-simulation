#include <iostream>
#include <vector>
#include <time.h>
#include <map>
#include <fstream>

using namespace std;

void getTerrorists(unsigned int hotelnum, unsigned int prob, unsigned int peoplenum, unsigned int daysnum)
{
	map<pair<int, int>, int> meetingmap;
	int max = 0;
	fstream file;
	file.open("hotel.log", std::fstream::out);


	for (unsigned int i = 0; i < daysnum; i++)
	{
		//std::cout << "Day " << i + 1 << endl;
		map<int, vector<int>> hotelmap;
		for (unsigned int p = 0; p < peoplenum; p++)
		{
			// Go to hotel
			if (rand() % 100 < prob)
			{
				int choice = rand() % hotelnum;
				hotelmap[choice].push_back(p);
				file << "Dzien " << i + 1 << " | Klient " << p + 1 << " odwiedza hotel " << choice + 1 << endl;
			}


		}
		//for(auto hotel : hotelmap)
		for (map<int, vector<int>>::iterator it = hotelmap.begin(); it != hotelmap.end(); ++it)
		{
			for (unsigned int j = 0; j < it->second.size(); j++)
			{
				for (unsigned int k = j + 1; k < it->second.size(); k++)
				{
					unsigned int p1, p2;
					p1 = it->second[j];
					p2 = it->second[k];
					auto p = make_pair(p1, p2);
					if (meetingmap.find(p) == meetingmap.end())
					{
						meetingmap[p] = 1;
					}
					else
					{
						meetingmap[p]++;
						if (max < meetingmap[p])
							max = meetingmap[p];
					}
				}
			}
		}
	}

	if (max > 1)
	{
		int *pairtab = new int[max - 1];

		for (int i = 0; i < max - 1; i++)
			pairtab[i] = 0;

		map<int, int> differentpersonsmap;
		for (map<pair<int, int>, int>::iterator it = meetingmap.begin(); it != meetingmap.end(); ++it)
		{
			if (it->second > 1)
			{
				pairtab[it->second - 2]++;
				if (differentpersonsmap.find(it->first.first) == differentpersonsmap.end())
					differentpersonsmap[it->first.first];
				if (differentpersonsmap.find(it->first.second) == differentpersonsmap.end())
					differentpersonsmap[it->first.second];
			}

		}

		for (int i = 0; i < max - 1; i++)
		{
			std::cout << i + 2 << " par: " << pairtab[i] << endl;
		}
		std::cout << "Roznych terrorystow: " << differentpersonsmap.size() << endl;
		
		file << endl << endl << "Podejrzani klienci: ";
		for (map<int, int>::iterator it = differentpersonsmap.begin(); it != differentpersonsmap.end(); ++it)
			file << it->first << ", ";
		
	}

	file.close();

}

int main()
{
	srand(time(NULL));
	// hotel, pdb, people, days
	getTerrorists(100, 30, 1000, 100);
	std::cout << "E.N.D.";
	getchar();
	return 0;
}